<%@ page import="com.Alchuk.model.entities.train.Train" %>
<%@ page import="java.util.List" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.Alchuk.model.service.TrainServiceImpl" %><%--
  Created by IntelliJ IDEA.
  User: ivan
  Date: 05.05.20
  Time: 19:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Trains</title>
</head>
<body>

<form action="trainController" method="post">
    <table style="width: 50%">
        <tr>
            <td>
                <label>Train Number:
                    <input type="text" name="trainNumber"><br/>
                </label>
            </td>

        </tr>

        <tr>
            <td>
                <label>Train Type:
                    <input type="text" name="trainType"><br/>
                </label>
            </td>

        </tr>

    </table>

    <input type="submit" name="action" value="add train"/>
    <input type="submit" name="action" value="train list"/>
</form>
<div>
    <button onclick="location.href = '/java_project_epam_war_exploded/adminMenuController'">Back to menu</button>
</div>

<%
    List<Train> trainList = (ArrayList<Train>) request.getAttribute("trains");


    if (trainList != null) {
        for (Train train : trainList) {
            out.println(train.toString() + "<br/>");
        }
    }

    String error = (String) request.getAttribute("error");
    String info = (String) request.getAttribute("info");
    out.println(error != null ? error : "");
    out.println(info != null ? info : "" );


%>
</body>
</html>
