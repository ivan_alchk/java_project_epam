<%@ page import="com.Alchuk.model.entities.route.Route" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.Alchuk.model.entities.train.Train" %><%--
  Created by IntelliJ IDEA.
  User: ivan
  Date: 20.04.20
  Time: 13:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Main Page</title>
</head>
<body>


<form action="userMenuController" method="post">
    <input type="submit" name="action" value="See available routes"/>
    <input type="submit" name="action" value="My routes">

    <label>Route to add:
        <input type="text" name="routeToAdd">
    </label>
    <input type="submit" name="action" value="add route">

    <label>Route to delete:
        <input type="text" name="routeToDelete">
    </label>
    <input type="submit" name="action" value="delete route">



</form>
<div>
    <button onclick="location.href = '/java_project_epam_war_exploded/userInfoController'">Change info</button>
</div>


<%
    List<Route> routes = (ArrayList<Route>) request.getAttribute("routes");
    String error = (String) request.getAttribute("error");
    String info = (String) request.getAttribute("info");


    out.println(info != null ? info : "");

    out.println(error != null ? error : "");

    if (routes != null) {
        for (Route route : routes) {
            out.println(route.toString() + "<br/>");
        }

    }


%>
</body>
</html>
