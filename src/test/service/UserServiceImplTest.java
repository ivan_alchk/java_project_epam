package service;

import com.Alchuk.model.dataLayer.dao.UserDao;
import com.Alchuk.model.entities.user.User;
import static org.junit.jupiter.api.Assertions.*;
import com.Alchuk.model.service.UserService;
import com.Alchuk.model.service.UserServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

class UserServiceImplTest {

    static UserDao userDao;
    static UserService userService;
    static User user;
    

    @BeforeAll
    static void setUp(){


        userDao = mock(UserDao.class);
        userService = new UserServiceImpl(userDao);
        user = new User("me", "secret", "firstName", "lastName", "123456789");

    }

    @Test
    void addUser() throws SQLException, ClassNotFoundException {

        userService.addUser(user);
        verify(userDao).addUser(user);



    }

    @Test
    void createUser() {
        userService.createUser("me", "secret", "firstName", "lastName", "123456789");
        assertEquals(user, userService.createUser("me", "secret", "firstName", "lastName", "123456789"));

    }

    @Test
    void findUserByUserName() throws SQLException, ClassNotFoundException {

        when(userDao.findUserByUserName("me")).thenReturn(true);

        assertTrue(userService.findUserByUserName("me"));

    }

    @Test
    void findUserByUserNameAndPassword() throws SQLException, ClassNotFoundException {
        when(userDao.findUserByUserNameAndPassword("me", "secret")).thenReturn(true);
        userService.findUserByUserNameAndPassword("me", "secret");
        assertTrue(userService.findUserByUserNameAndPassword("me", "secret"));
    }

    @Test
    void deleteByUserName() throws SQLException, ClassNotFoundException {
        userService.deleteByUserName("me");
        verify(userDao).deleteByUserName("me");

    }

    @Test
    void updateUserName() throws SQLException, ClassNotFoundException {
        userService.updateUserName("newMe", "me");
        verify(userDao).updateUserName("newMe", "me");
    }

    @Test
    void updatePassword() throws SQLException, ClassNotFoundException {
        userService.updatePassword("me", "newSecret");
        verify(userDao).updatePassword("me", "newSecret");
    }

    @Test
    void getAllUserRoutes() throws SQLException, ClassNotFoundException {
        List<Integer> testRoutes = new ArrayList<>();
        when(userDao.getAllUserRoutes("me")).thenReturn(testRoutes);

        userService.getAllUserRoutes("me");

        assertEquals(testRoutes, userService.getAllUserRoutes("me"));
    }

    @Test
    void setRouteToUser() throws SQLException, ClassNotFoundException {
        userService.setRouteToUser(0, "me");
        verify(userDao).setRouteToUser(0, "me");
    }


    @Test
    void deleteUserRoute() throws SQLException, ClassNotFoundException {
        userService.deleteUserRoute(0, "me");
        verify(userDao).deleteUserRoute("me", 0);
    }
}