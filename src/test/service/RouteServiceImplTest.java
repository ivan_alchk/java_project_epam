package service;

import com.Alchuk.model.dataLayer.dao.RouteDao;
import com.Alchuk.model.entities.route.Route;
import com.Alchuk.model.entities.train.Shinkansen;
import com.Alchuk.model.entities.train.Train;
import com.Alchuk.model.service.RouteServiceImpl;

import org.junit.jupiter.api.BeforeAll;

import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class RouteServiceImplTest {


    static RouteDao routeDao;
    static RouteServiceImpl routeService;
    static Route route;
    static Train train;

    @BeforeAll
    static void setUp() {
        routeDao = mock(RouteDao.class);
        routeService = new RouteServiceImpl(routeDao);
        train = new Shinkansen("S000000");
        route = new Route(0, "CityA", "CityB", 100, "12", train);
        route.calculateArrivalTime();
    }

    @Test
    void addRoute() throws SQLException, ClassNotFoundException {
        routeService.addRoute(route);


        verify(routeDao).addRoute(route);
    }

    @Test
    void findRouteByID() throws SQLException, ClassNotFoundException {

        when(routeDao.findRouteByID(0)).thenReturn(true);
        routeService.findRouteByID(0);


        verify(routeDao).findRouteByID(0);
        assertTrue(routeService.findRouteByID(0));

    }

    @Test
    void getAllRoutes() throws SQLException, ClassNotFoundException {
        List<Route> testRouteList = new ArrayList<>();
        when(routeDao.getAllRoutes()).thenReturn(testRouteList);

        routeService.getAllRoutes();
        assertEquals(testRouteList, routeService.getAllRoutes());

    }

    @Test
    void createRoute() {
        assertEquals(route, routeService.createRoute("0", "CityA", "CityB", "12", "100", train));
    }
}