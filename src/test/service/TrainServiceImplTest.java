package service;

import com.Alchuk.model.dataLayer.dao.TrainDao;
import com.Alchuk.model.entities.train.Shinkansen;
import com.Alchuk.model.entities.train.Train;
import com.Alchuk.model.service.TrainService;
import com.Alchuk.model.service.TrainServiceImpl;
import org.junit.jupiter.api.BeforeAll;

import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TrainServiceImplTest {

    static TrainDao trainDao;
    static Train testTrain;
    static TrainService trainService;

    @BeforeAll
    static void setUp() {
        trainDao = mock(TrainDao.class);
        testTrain = new Shinkansen("S000000");
        trainService = new TrainServiceImpl(trainDao);
    }

    @Test
    void addTrain() throws SQLException, ClassNotFoundException {
        trainService.addTrain(testTrain);
        verify(trainDao).addTrain(testTrain);

    }

    @Test
    void findTrainByNumber() throws SQLException, ClassNotFoundException {
        when(trainDao.findTrainByNumber("S000000")).thenReturn(testTrain);
        trainService.findTrainByNumber("S000000");
        assertEquals(testTrain, trainService.findTrainByNumber("S000000"));
    }

    @Test
    void getAll() throws SQLException, ClassNotFoundException {
        List<Train> testTrainList = new ArrayList<>();
        when(trainDao.getAll()).thenReturn(testTrainList);
        trainService.getAll();
        assertEquals(testTrainList, trainService.getAll());
    }

    @Test
    void createTrain() {
        trainService.createTrain("S000000", "S");
        assertEquals(testTrain, trainService.createTrain("S000000", "S"));
    }

    @Test
    void updateAvailability() throws SQLException, ClassNotFoundException {
        trainService.updateAvailability("S000000");
        verify(trainDao).updateAvailability("S000000");

    }
}