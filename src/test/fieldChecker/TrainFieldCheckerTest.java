package fieldChecker;

import com.Alchuk.model.exceptions.InvalidInputException;
import com.Alchuk.model.fieldChecker.TrainFieldChecker;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TrainFieldCheckerTest {

    @Test
    void checkInputFields() {
        try {
            TrainFieldChecker.checkInputFields("AA000000", "S");
            fail();
        }catch (InvalidInputException ignored){

        }
    }
}