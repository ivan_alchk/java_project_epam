package fieldChecker;

import com.Alchuk.model.exceptions.InvalidInputException;
import com.Alchuk.model.fieldChecker.UserFieldChecker;
import org.h2.engine.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserFieldCheckerTest {

    String userName = "me";
    String password = "1";
    String lastName = "last1";
    String firstName = "first1";
    String telephoneNumber = "122442t";



    @Test
    void checkUserName() {
        try{
            UserFieldChecker.checkUserName(userName);
            fail();
        }catch (InvalidInputException ignored){

        }

    }

    @Test
    void checkUserPassword() {
        try{
            UserFieldChecker.checkUserPassword(password);
            fail();
        }catch (InvalidInputException ignored){

        }
    }

    @Test
    void checkUserFirstNameAndLastName() {
        try{
            UserFieldChecker.checkUserFirstNameAndLastName(firstName, lastName);
            fail();
        }catch (InvalidInputException ignored){

        }
    }

    @Test
    void checkUserTelephoneNumber() {
        try{
            UserFieldChecker.checkUserTelephoneNumber(telephoneNumber);
            fail();
        }catch (InvalidInputException ignored){

        }
    }
}