package fieldChecker;

import com.Alchuk.model.exceptions.InvalidInputException;
import com.Alchuk.model.fieldChecker.RouteFieldChecker;
import com.Alchuk.model.fieldChecker.TrainFieldChecker;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RouteFieldCheckerTest {

    @Test
    void checkInputFields() {
        try{
            RouteFieldChecker.checkInputFields("a1", "A", "A", "sss", "110a");
            fail();
        }catch (InvalidInputException ignored){

        }

    }
}