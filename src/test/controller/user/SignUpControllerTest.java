package controller.user;

import com.Alchuk.controller.user.SignUpController;

import com.Alchuk.model.defaultMessages.Messages;
import com.Alchuk.model.entities.user.User;
import com.Alchuk.model.exceptions.UserAlreadyExistsException;
import com.Alchuk.model.fieldChecker.UserFieldChecker;
import com.Alchuk.model.service.UserService;
import org.junit.jupiter.api.BeforeAll;

import org.junit.jupiter.api.Test;









import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.SQLException;


import static org.mockito.Mockito.*;


class SignUpControllerTest {

    static HttpServletRequest request;
    static HttpServletResponse response;
    static RequestDispatcher requestDispatcher;
    static ServletConfig servletConfig;
    static UserService userService;

    static User user;


    static SignUpController signUpController;


    @BeforeAll
    static void setUp() throws ServletException {

        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        requestDispatcher = mock(RequestDispatcher.class);
        servletConfig = mock(ServletConfig.class);


        userService = mock(UserService.class);

        user = new User("user_UserName", "password", "me", "lastName", "123456789");

        when(request.getRequestDispatcher("JSP/SignUp.jsp")).thenReturn(requestDispatcher);

        signUpController = new SignUpController();

        signUpController.init(servletConfig);


    }

    @Test
    void doGet() throws ServletException, IOException {

        signUpController.doGet(request, response);

        verify(requestDispatcher, times(2)).forward(request, response);



    }

    @Test
    void doPost() throws ServletException, IOException, SQLException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
        when(request.getParameter("userName")).thenReturn("user_UserName");
        when(request.getParameter("pass")).thenReturn("password");
        when(request.getParameter("firstName")).thenReturn("me");
        when(request.getParameter("lastName")).thenReturn("lastName");
        when(request.getParameter("telephoneNumber")).thenReturn("1234567890");
        when(userService.createUser("user_UserName", "password", "me", "lastName", "1234567890")).thenReturn(user);


        Field field = signUpController.getClass().getDeclaredField("userService");
        field.setAccessible(true);
        field.set(signUpController, userService);
        signUpController.doPost(request, response);

        verify(userService).createUser("user_UserName", "password", "me", "lastName", "1234567890");
        verify(userService).addUser(user);
        verify(request).setAttribute("correctInput", Messages.USER_ADDED.getMessage());




    }
}