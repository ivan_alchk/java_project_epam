package controller.user;

import com.Alchuk.controller.user.UserInfoController;
import com.Alchuk.model.service.UserService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.SQLException;

import static org.mockito.Mockito.*;


class UserInfoControllerTest {

    static HttpServletRequest request;
    static HttpServletResponse response;
    static RequestDispatcher requestDispatcher;
    static UserInfoController userInfoController;
    static ServletConfig servletConfig;
    static HttpSession session;
    static UserService userService;

    @BeforeAll
    static void setUp() throws ServletException {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        requestDispatcher = mock(RequestDispatcher.class);
        servletConfig = mock(ServletConfig.class);
        session = mock(HttpSession.class);
        userService = mock(UserService.class);

        userInfoController = new UserInfoController();
        userInfoController.init(servletConfig);

        when(request.getRequestDispatcher("JSP/UserInfo.jsp")).thenReturn(requestDispatcher);
    }

    @Test
    void doGet() throws ServletException, IOException {

        userInfoController.doGet(request, response);
        verify(requestDispatcher).forward(request, response);

    }

    @Test
    void doPost() throws ServletException, IOException, NoSuchFieldException, IllegalAccessException, SQLException, ClassNotFoundException {
        when(request.getParameter("action")).thenReturn("Delete your account");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userName")).thenReturn("me");



        Field userServiceField = userInfoController.getClass().getDeclaredField("userService");
        userServiceField.setAccessible(true);
        userServiceField.set(userInfoController, userService);


                userInfoController.doPost(request, response);

        verify(userService, only()).deleteByUserName("me");
        verify(response, only()).sendRedirect("JSP/LogIn.jsp");


    }
}