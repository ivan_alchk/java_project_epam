package controller.user;

import com.Alchuk.controller.user.UserMenuController;
import com.Alchuk.model.defaultMessages.Messages;
import com.Alchuk.model.service.RouteService;
import com.Alchuk.model.service.UserService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.SQLException;
import java.util.Arrays;

import static org.mockito.Mockito.*;


class UserMenuControllerTest {


    static HttpServletResponse response;
    static HttpServletRequest request;
    static RequestDispatcher requestDispatcher;
    static ServletConfig servletConfig;
    static UserMenuController userMenuController;
    static UserService userService;
    static RouteService routeService;
    static HttpSession session;


    @BeforeAll
    static void setUp() throws ServletException {
        response = mock(HttpServletResponse.class);
        request = mock(HttpServletRequest.class);
        requestDispatcher = mock(RequestDispatcher.class);
        servletConfig = mock(ServletConfig.class);
        userService = mock(UserService.class);
        routeService = mock(RouteService.class);
        session = mock(HttpSession.class);

        userMenuController = new UserMenuController();
        userMenuController.init(servletConfig);

        when(request.getRequestDispatcher("JSP/UserMenu.jsp")).thenReturn(requestDispatcher);



    }



    @Test
    void doGet() throws ServletException, IOException {

        userMenuController.doGet(request, response);
        verify(requestDispatcher, times(2)).forward(request, response);

    }

    @Test
    void doPost() throws ServletException, IOException, SQLException, ClassNotFoundException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userName")).thenReturn("me");
        when(request.getParameter("action")).thenReturn("add route");
        when(request.getParameter("routeToAdd")).thenReturn("0");

        Field[] userMenuControllerFields = UserMenuController.class.getDeclaredFields();
        Arrays.stream(userMenuControllerFields).filter(field -> !Modifier.isStatic(field.getModifiers())).forEach(field -> {
            field.setAccessible(true);
            try {
                field.set(userMenuController, field.getName().equals("routeService") ? routeService : userService);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });

        userMenuController.doPost(request ,response);

        verify(routeService).getAllRoutes();
        verify(userService).setRouteToUser(0, "me");
        verify(request).setAttribute("info", Messages.ROUTE_ADDED.getMessage());



    }
}