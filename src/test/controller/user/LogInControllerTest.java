package controller.user;


import com.Alchuk.controller.user.LogInController;

import com.Alchuk.model.service.UserService;
import com.Alchuk.model.service.UserServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.lang.reflect.Field;

import java.sql.SQLException;

import static org.mockito.Mockito.*;


class LogInControllerTest {
    static HttpServletRequest req;
    static HttpServletResponse resp;
    static ServletConfig sg;
    static RequestDispatcher requestDispatcher;
    static LogInController logInController;
    static HttpSession session;
    static UserService userService;


    @BeforeAll
    static void setUp() throws ServletException {

        req = mock(HttpServletRequest.class);
        resp = mock(HttpServletResponse.class);
        sg = mock(ServletConfig.class);
        requestDispatcher = mock(RequestDispatcher.class);
        session = mock(HttpSession.class);
        userService = mock(UserServiceImpl.class);

        when(req.getRequestDispatcher("JSP/LogIn.jsp")).thenReturn(requestDispatcher);

        logInController = new LogInController();
        logInController.init(sg);


    }


    @Test
    void doGet() throws ServletException, IOException {

        logInController.doGet(req, resp);

        verify(requestDispatcher, only()).forward(req, resp);


    }

    @Test
    void doPost() throws ServletException, IOException, SQLException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException {


        when(req.getParameter("name")).thenReturn("me");
        when(req.getParameter("pass")).thenReturn("password");
        when(userService.findUserByUserNameAndPassword("me", "password")).thenReturn(true);
        when(req.getSession()).thenReturn(session);

        Field field = logInController.getClass().getDeclaredField("userService");
        field.setAccessible(true);
        field.set(logInController, userService);

        logInController.doPost(req, resp);
        verify(userService).findUserByUserNameAndPassword("me", "password");
        verify(session, only()).setAttribute("userName", "me");
        verify(resp, only()).sendRedirect("userMenuController");


    }



}