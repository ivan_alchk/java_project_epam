package controller.admin;


import com.Alchuk.controller.admin.RouteController;
import com.Alchuk.controller.admin.TrainController;
import com.Alchuk.model.defaultMessages.Messages;
import com.Alchuk.model.entities.train.Train;
import com.Alchuk.model.service.TrainService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.SQLException;

import static org.mockito.Mockito.*;

class TrainControllerTest {

    static HttpServletRequest request;
    static HttpServletResponse response;
    static ServletConfig config;
    static RequestDispatcher requestDispatcher;
    static TrainService trainService;
    static TrainController trainController;

    @BeforeEach
    void setUp() throws ServletException {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        requestDispatcher = mock(RequestDispatcher.class);
        config = mock(ServletConfig.class);
        trainService = mock(TrainService.class);

        trainController = new TrainController();
        trainController.init(config);

        when(request.getRequestDispatcher("JSP/Train.jsp")).thenReturn(requestDispatcher);

    }

    @Test
    void doGet() throws ServletException, IOException {

        trainController.doGet(request, response);

        verify(requestDispatcher, times(1)).forward(request, response);

    }

    @Test
    void doPost() throws NoSuchFieldException, IllegalAccessException, SQLException, ClassNotFoundException, ServletException, IOException {


        Train testTrain = mock(Train.class);


        when(request.getParameter("action")).thenReturn("add train");
        when(request.getParameter("trainNumber")).thenReturn("S000000");
        when(request.getParameter("trainType")).thenReturn("S");
        when(trainService.findTrainByNumber("S000000")).thenReturn(null);
        when(trainService.createTrain("S000000", "S")).thenReturn(testTrain);


        Field trainControllerField = TrainController.class.getDeclaredField("trainService");
        trainControllerField.setAccessible(true);
        trainControllerField.set(trainController, trainService);

        trainController.doPost(request, response);

        verify(trainService).findTrainByNumber("S000000");
        verify(trainService).createTrain("S000000", "S");
        verify(request).setAttribute("info", Messages.TRAIN_ADDED.getMessage());


    }
}