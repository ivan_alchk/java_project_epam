package controller.admin;

import com.Alchuk.controller.admin.RouteController;
import com.Alchuk.model.defaultMessages.Messages;
import com.Alchuk.model.entities.route.Route;
import com.Alchuk.model.entities.train.Shinkansen;
import com.Alchuk.model.entities.train.Train;
import com.Alchuk.model.service.RouteService;
import com.Alchuk.model.service.TrainService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.SQLException;
import java.util.Arrays;

import static org.mockito.Mockito.*;

class RouteControllerTest {

    static HttpServletRequest request;
    static HttpServletResponse response;
    static RequestDispatcher requestDispatcher;
    static ServletConfig servletConfig;
    static RouteController routeController;
    static TrainService trainService;
    static Train testTrain;
    static RouteService routeService;
    static Route route;


    @BeforeAll
    static void setUp() throws ServletException {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        requestDispatcher = mock(RequestDispatcher.class);
        servletConfig = mock(ServletConfig.class);
        routeController = new RouteController();
        routeController.init(servletConfig);
        trainService = mock(TrainService.class);
        routeService = mock(RouteService.class);
        route = mock(Route.class);



        testTrain = new Shinkansen("S000000");
        testTrain.setAvailability(true);

        when(request.getRequestDispatcher("JSP/Route.jsp")).thenReturn(requestDispatcher);
    }

    @Test
    void doGet() throws ServletException, IOException {
        routeController.doGet(request, response);
        verify(requestDispatcher, times(2)). forward(request, response);
    }

    @Test
    void doPost() throws SQLException, ClassNotFoundException, ServletException, IOException {
        when(request.getParameter("routeId")).thenReturn("12");
        when(request.getParameter("fromCity")).thenReturn("CityA");
        when(request.getParameter("toCity")).thenReturn("CityB");
        when(request.getParameter("departureTime")).thenReturn("10");
        when(request.getParameter("trainNumber")).thenReturn("S000000");
        when(request.getParameter("distance")).thenReturn("100");
        when(trainService.findTrainByNumber("S000000")).thenReturn(testTrain);
        when(routeService.createRoute("12", "CityA", "CityB", "10", "100", testTrain)).thenReturn(route);


        Field[] controllerFields = RouteController.class.getDeclaredFields();
        Arrays.stream(controllerFields).filter(field -> !Modifier.isStatic(field.getModifiers())).forEach(field -> {
            try {
                field.setAccessible(true);
                field.set(routeController, field.getName().equals("routeService") ? routeService : trainService);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });

        routeController.doPost(request, response);

        verify(trainService).updateAvailability(anyString());
        verify(routeService).createRoute("12", "CityA", "CityB", "10", "100", testTrain);
        verify(routeService).addRoute(route);
        verify(request).setAttribute("info", Messages.ROUTE_ADDED.getMessage());



    }
}