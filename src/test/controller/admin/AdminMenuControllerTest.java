package controller.admin;

import com.Alchuk.controller.admin.AdminMenuController;
import org.junit.jupiter.api.BeforeAll;

import org.junit.jupiter.api.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.mockito.Mockito.*;


class AdminMenuControllerTest {

    static HttpServletResponse response;
    static HttpServletRequest request;
    static ServletConfig servletConfig;
    static RequestDispatcher requestDispatcher;
    static AdminMenuController adminMenuController;


    @BeforeAll
    static void setUp() throws ServletException {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        requestDispatcher = mock(RequestDispatcher.class);
        servletConfig = mock(ServletConfig.class);

        adminMenuController = new AdminMenuController();
        adminMenuController.init(servletConfig);

        when(request.getRequestDispatcher("JSP/AdminMenu.jsp")).thenReturn(requestDispatcher);

    }

    @Test
    void doGet() throws ServletException, IOException {
        adminMenuController.doGet(request, response);
        verify(requestDispatcher, times(2)).forward(request, response);
    }

    @Test
    void doPost() throws ServletException, IOException {
        adminMenuController.doPost(request, response);

    }
}