package trainFactory;

import com.Alchuk.model.entities.train.GeneralTrain;
import com.Alchuk.model.entities.train.Shinkansen;
import com.Alchuk.model.entities.train.Train;
import com.Alchuk.model.trainFactory.TrainFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TrainFactoryTest {



    static TrainFactory trainFactory;

    @BeforeAll
    static void setUp(){

        trainFactory = TrainFactory.getTrainFactory();
    }

    @Test
    void createTrainByTrainNumber() {

        assertEquals("S", trainFactory.createTrainByTrainNumber("S000000").getTrainType());
        assertEquals("GT", trainFactory.createTrainByTrainNumber("GT000000").getTrainType());
    }

    @Test
    void createTrainByType() {
        assertNotEquals("GT", trainFactory.createTrainByType("S", "S000000").getTrainType());
    }
}