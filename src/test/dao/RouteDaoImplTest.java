package dao;

import com.Alchuk.model.dataLayer.dao.RouteDao;
import com.Alchuk.model.dataLayer.daoImpl.RouteDaoImpl;
import com.Alchuk.model.entities.route.Route;
import com.Alchuk.model.entities.train.Shinkansen;
import com.Alchuk.model.entities.train.Train;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RouteDaoImplTest {

    static RouteDao routeDao;
    static Route route;
    static Train train;

    @BeforeAll
    static void setUp() throws SQLException, ClassNotFoundException {
        routeDao = new RouteDaoImpl();
        train = new Shinkansen("S000000");
        route = new Route(0, "CityA", "CityB", 100, "12", train);
    }

    @AfterEach
    void tearDown() throws SQLException, ClassNotFoundException {
        routeDao.clearAllDataFromTable();

    }


    @Test
    void addRoute() throws SQLException, ClassNotFoundException {

        routeDao.addRoute(route);
        assertTrue(routeDao.findRouteByID(0));


    }

    @Test
    void findRouteByID() throws SQLException, ClassNotFoundException {
        routeDao.addRoute(route);
        assertTrue(routeDao.findRouteByID(0));
        assertFalse(routeDao.findRouteByID(1));

    }

    @Test
    void getAllRoutes() throws SQLException, ClassNotFoundException {
        routeDao.addRoute(route);
        List<Route> testRouteList = routeDao.getAllRoutes();

        assertEquals(1, testRouteList.size());

    }
}