package dao;

import com.Alchuk.model.dataLayer.dao.TrainDao;
import com.Alchuk.model.dataLayer.daoImpl.TrainDaoImpl;
import com.Alchuk.model.entities.train.Shinkansen;
import com.Alchuk.model.entities.train.Train;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TrainDaoImplTest {



    static TrainDao trainDao;
    static Train testTrain;




    @BeforeAll
    static void setUp() throws SQLException, ClassNotFoundException {
        trainDao = new TrainDaoImpl();
        testTrain = new Shinkansen("S000000");

    }

    @BeforeEach
    void setTrain() throws SQLException, ClassNotFoundException {
        trainDao. addTrain(testTrain);
    }

    @AfterEach
    void tearDown() throws SQLException, ClassNotFoundException {
        trainDao.clearAllDataFromTable();
    }

    @Test
    void addTrain() throws SQLException, ClassNotFoundException {

        assertEquals("S000000", trainDao.findTrainByNumber("S000000").getTrainNumber());
    }

    @Test
    void checkIfTrainAvailable() throws SQLException, ClassNotFoundException {
        assertTrue(trainDao.checkIfTrainAvailable("S000000"));
    }

    @Test
    void findTrainByNumber() throws SQLException, ClassNotFoundException {
        assertNull(trainDao.findTrainByNumber("S000001"));
    }

    @Test
    void getAll() throws SQLException, ClassNotFoundException {
        List<Train> testTrainList = trainDao.getAll();

        assertEquals(1, testTrainList.size());
    }

    @Test
    void updateAvailability() throws SQLException, ClassNotFoundException {
        trainDao.updateAvailability("S000000");
        assertFalse(trainDao.checkIfTrainAvailable("S000000"));
    }
}