package dao;

import com.Alchuk.model.dataLayer.dao.UserDao;
import com.Alchuk.model.dataLayer.daoImpl.UserDaoImpl;
import com.Alchuk.model.entities.user.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class UserDaoImplTest {

    static UserDao userDao;
    static User user;


    @BeforeAll
    static void setUp() throws SQLException, ClassNotFoundException {
        userDao = new UserDaoImpl();
        user = new User("me", "secret", "firstName", "lastName", "123456789");
    }

    @BeforeEach
    void setUser() throws SQLException, ClassNotFoundException {
        userDao.addUser(user);
    }
    @AfterEach
    void tearDown() throws SQLException, ClassNotFoundException {
        userDao.clearAllDataFromTable();
    }

    @Test
    void addUser() throws SQLException, ClassNotFoundException {


        assertTrue(userDao.findUserByUserName("me"));
        assertTrue(userDao.findUserByUserNameAndPassword("me", "secret"));


    }

    @Test
    void findUserByUserName() throws SQLException, ClassNotFoundException {


        assertTrue(userDao.findUserByUserName("me"));
        assertFalse(userDao.findUserByUserName("you"));
    }

    @Test
    void findUserByUserNameAndPassword() throws SQLException, ClassNotFoundException {


        assertFalse(userDao.findUserByUserNameAndPassword("you", "secret"));
        assertFalse(userDao.findUserByUserNameAndPassword("me", "yourSecret"));
        assertTrue(userDao.findUserByUserNameAndPassword("me", "secret"));
    }

    @Test
    void deleteByUserName() throws SQLException, ClassNotFoundException {

        userDao.deleteByUserName("me");

        assertFalse(userDao.findUserByUserName("me"));
        assertFalse(userDao.findUserByUserNameAndPassword("me", "secret"));

    }

    @Test
    void updateUserName() throws SQLException, ClassNotFoundException {

        userDao.updateUserName("meme", "me");


        assertFalse(userDao.findUserByUserName("me"));
        assertTrue(userDao.findUserByUserName("meme"));


    }

    @Test
    void updatePassword() throws SQLException, ClassNotFoundException {

        userDao.updatePassword("me", "newSecret");
        assertFalse(userDao.findUserByUserNameAndPassword("me", "secret"));
        assertTrue(userDao.findUserByUserNameAndPassword("me", "newSecret"));


    }

    @Test
    void setRouteToUser() throws SQLException, ClassNotFoundException {
        userDao.setRouteToUser(0, "me");
        assertEquals(0, userDao.getAllUserRoutes("me").get(0));
    }

    @Test
    void getAllUserRoutes() throws SQLException, ClassNotFoundException {
        userDao.setRouteToUser(0, "me");

        assertEquals(1, userDao.getAllUserRoutes("me").size());
    }

    @Test
    void deleteUserRoute() throws SQLException, ClassNotFoundException {
        userDao.setRouteToUser(0, "me");
        userDao.deleteUserRoute("me", 0);
        assertEquals(0, userDao.getAllUserRoutes("me").size());
    }
}