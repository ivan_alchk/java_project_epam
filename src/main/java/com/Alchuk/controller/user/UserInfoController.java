package com.Alchuk.controller.user;

import com.Alchuk.model.fieldChecker.UserFieldChecker;
import com.Alchuk.model.defaultMessages.Messages;
import com.Alchuk.model.errorMessages.ErrorMessages;
import com.Alchuk.model.exceptions.InvalidInputException;
import com.Alchuk.model.injecotrs.UserServiceInjector;
import com.Alchuk.model.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class UserInfoController extends HttpServlet {

    private final UserService userService;
    private static final Logger logger = LogManager.getLogger(UserInfoController.class);


    public UserInfoController() {
        userService = UserServiceInjector.getUserServiceInjector().inject();
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("JSP/UserInfo.jsp");
        requestDispatcher.forward(req, resp);

    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = (String) req.getSession().getAttribute("userName");
        String action = req.getParameter("action");

        try {
            if (action.equals("Delete your account")) {
                userService.deleteByUserName(userName.trim());
                logger.info(userName + " " + Messages.USER_DELETED.getMessage());
                resp.sendRedirect("JSP/LogIn.jsp");
                return;
            }
            if (action.equals("Update username")) {
                String newUserName = req.getParameter("newUserName");
                UserFieldChecker.checkUserName(newUserName);
                userService.updateUserName(newUserName, userName.trim());
                req.getSession().setAttribute("userName", newUserName.trim());
                userName = newUserName.trim();
                logger.info(Messages.USER_NAME_CHANGED.getMessage() + " " + newUserName);
            }
            if(action.equals("Update password")){
                String newPassword = req.getParameter("newPassword");
                UserFieldChecker.checkUserPassword(newPassword.trim());
                userService.updatePassword(userName.trim(), newPassword.trim());
                logger.info(userName + " " + Messages.PASSWORD_CHANGED.getMessage());
            }
        } catch (ClassNotFoundException | SQLException e) {
            logger.error(ErrorMessages.DEFAULT_EXCEPTION.getMessage());
            e.printStackTrace();
        }catch (InvalidInputException e){
            logger.error(e.getMessage());

        }


        doGet(req, resp);

    }


}
