package com.Alchuk.controller.user;


import com.Alchuk.model.defaultMessages.Messages;
import com.Alchuk.model.entities.route.Route;
import com.Alchuk.model.errorMessages.ErrorMessages;
import com.Alchuk.model.injecotrs.RouteServiceInjector;
import com.Alchuk.model.injecotrs.UserServiceInjector;
import com.Alchuk.model.service.RouteService;
import com.Alchuk.model.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class UserMenuController extends HttpServlet {

    private final RouteService routeService;
    private final UserService userService;

    private static final Logger logger = LogManager.getLogger(UserMenuController.class);

    public UserMenuController() {
        routeService = RouteServiceInjector.getRouteServiceInjector().inject();
        userService = UserServiceInjector.getUserServiceInjector().inject();
    }


    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("JSP/UserMenu.jsp");
        requestDispatcher.forward(req, resp);


    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String action = req.getParameter("action");
        String userName = (String) req.getSession().getAttribute("userName");
        if(action != null){
            try {
                List<Route> routeList = routeService.getAllRoutes();
                if (action.equals("See available routes")) {
                    req.setAttribute("routes", routeList);
                }

                if(action.equals("My routes")){
                    List<Integer> routesId = userService.getAllUserRoutes(userName.trim());
                    List<Route> userRoutes = new ArrayList<>();
                    for(Integer id : routesId){
                        for(Route route : routeList){
                            if(id == route.getId()){
                                userRoutes.add(route);
                            }
                        }
                    }
                    if(userRoutes.size() == 0){
                        req.setAttribute("info", "You have not added any routes");
                    }
                    req.setAttribute("routes", userRoutes);

                }
                if(action.equals("add route")){
                    String routeIdToAdd = req.getParameter("routeToAdd");
                    userService.setRouteToUser(Integer.parseInt(routeIdToAdd), userName.trim());
                    logger.info(userName + " " + Messages.ROUTE_ADDED.getMessage());
                    req.setAttribute("info", Messages.ROUTE_ADDED.getMessage());
                }
                if(action.equals("delete route")){
                    String routeIdToDelete = req.getParameter("routeToDelete");
                    userService.deleteUserRoute(Integer.parseInt(routeIdToDelete), userName.trim());
                    logger.info(userName + " " + Messages.ROUTE_DELETED.getMessage() + " " + routeIdToDelete);
                    req.setAttribute("info", Messages.ROUTE_DELETED.getMessage() + " " + routeIdToDelete);
                }
            } catch (SQLException | ClassNotFoundException e) {
                req.setAttribute("error", ErrorMessages.DEFAULT_EXCEPTION.getMessage());
                logger.error(e.getMessage());

            }
        }


        doGet(req, resp);
    }
}

