package com.Alchuk.controller.user;


import com.Alchuk.model.errorMessages.ErrorMessages;
import com.Alchuk.model.exceptions.InvalidInputException;
import com.Alchuk.model.injecotrs.UserServiceInjector;
import com.Alchuk.model.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;


public class LogInController extends HttpServlet {

    private static final Logger logger = LogManager.getLogger(LogInController.class);

    private final UserService userService;

    public LogInController() {
        userService = UserServiceInjector.getUserServiceInjector().inject();

    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        RequestDispatcher requestDispatcher = req.getRequestDispatcher("JSP/LogIn.jsp");
        requestDispatcher.forward(req, resp);


    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String userName = req.getParameter("name");
        String password = req.getParameter("pass");

        try {


            if (userService.findUserByUserNameAndPassword(userName.trim(), password.trim())) {
                req.getSession().setAttribute("userName", userName);
                resp.sendRedirect("userMenuController");
                logger.info("User logged in`" + " " + userName);
                return;
            }

            throw new InvalidInputException(ErrorMessages.INCORRECT_INFO.getMessage());
        } catch (ClassNotFoundException | SQLException e) {
            req.setAttribute("incorrect", ErrorMessages.DEFAULT_EXCEPTION.getMessage());
            logger.error(e.getMessage() + userName);
        } catch (InvalidInputException e) {
            logger.error(e.getMessage() + " " + userName);
            req.setAttribute("incorrect", e.getMassage());
        }


        doGet(req, resp);


    }
}
