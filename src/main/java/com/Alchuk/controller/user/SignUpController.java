package com.Alchuk.controller.user;

import com.Alchuk.model.fieldChecker.UserFieldChecker;
import com.Alchuk.model.defaultMessages.Messages;
import com.Alchuk.model.entities.user.User;
import com.Alchuk.model.errorMessages.ErrorMessages;
import com.Alchuk.model.exceptions.InvalidInputException;
import com.Alchuk.model.exceptions.UserAlreadyExistsException;
import com.Alchuk.model.injecotrs.UserServiceInjector;
import com.Alchuk.model.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;


public class SignUpController extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(SignUpController.class);


    private final UserService userService;

    public SignUpController() {
        this.userService = UserServiceInjector.getUserServiceInjector().inject();
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("JSP/SignUp.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("userName");
        String password = req.getParameter("pass");
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String telephoneNumber = req.getParameter("telephoneNumber");


        try {
            if(userService.findUserByUserName(userName)){
                throw new UserAlreadyExistsException(ErrorMessages.USER_EXISTS.getMessage());

            }
            UserFieldChecker.checkInputFields(userName, password, firstName, lastName, telephoneNumber);
            User newUser = userService.createUser(userName, password, firstName, lastName, telephoneNumber);
            userService.addUser(newUser);
            req.setAttribute("correctInput", Messages.USER_ADDED.getMessage());
            logger.info(Messages.USER_ADDED.getMessage() + " " + userName);
        } catch (InvalidInputException e) {
            req.setAttribute("invalidInput", e.getMassage());
            logger.error("Invalid fields" + " " + userName + " " + e.getMassage());
        } catch (UserAlreadyExistsException e){
            req.setAttribute("invalidInput", e.getMessage());
            logger.error("Failed sign up:" + " " + userName + " " + e.getMessage() );

        }
        catch (ClassNotFoundException | SQLException e) {
            req.setAttribute("invalidInput", ErrorMessages.DEFAULT_EXCEPTION.getMessage());
            logger.error("Inner fail" +  " " + e.getMessage());

        }

        doGet(req, resp);

    }




}
