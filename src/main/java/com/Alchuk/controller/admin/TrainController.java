package com.Alchuk.controller.admin;


import com.Alchuk.model.defaultMessages.Messages;
import com.Alchuk.model.entities.train.Train;
import com.Alchuk.model.errorMessages.ErrorMessages;
import com.Alchuk.model.exceptions.InvalidInputException;
import com.Alchuk.model.exceptions.TrainWithSuchNumberAlreadyExistsException;
import com.Alchuk.model.fieldChecker.TrainFieldChecker;
import com.Alchuk.model.injecotrs.TrainServiceInjector;
import com.Alchuk.model.service.TrainService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;


public class TrainController extends HttpServlet {

    private final TrainService trainService;
    private static final Logger logger = LogManager.getLogger(TrainController.class);


    public TrainController() {
        trainService = TrainServiceInjector.getTrainServiceInjector().inject();
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        RequestDispatcher requestDispatcher = req.getRequestDispatcher("JSP/Train.jsp");
        requestDispatcher.forward(req, resp);

    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        String trainNumber = req.getParameter("trainNumber");
        String trainType = req.getParameter("trainType");
        try {
            if (action.equals("add train")) {


                if (trainService.findTrainByNumber(trainNumber) != null) {
                    throw new TrainWithSuchNumberAlreadyExistsException(ErrorMessages.TRAIN_EXISTS.getMessage());

                }
                TrainFieldChecker.checkInputFields(trainNumber, trainType);
                Train newTrain = trainService.createTrain(trainNumber, trainType);
                trainService.addTrain(newTrain);
                req.setAttribute("info", Messages.TRAIN_ADDED.getMessage());
                logger.info(Messages.TRAIN_ADDED + " " + trainNumber);

            }
            if (action.equals("train list")) {

                req.setAttribute("trains", trainService.getAll());


            }
        } catch (SQLException | ClassNotFoundException e) {
            req.setAttribute("error", ErrorMessages.DEFAULT_EXCEPTION.getMessage());

            logger.error("Inner fail" + e.getMessage());
        } catch (InvalidInputException e) {
            req.setAttribute("error", e.getMassage());
            logger.error("Invalid Input" + e.getMassage());
        } catch (TrainWithSuchNumberAlreadyExistsException e) {
            req.setAttribute("error", e.getMessage());
            logger.error(ErrorMessages.TRAIN_EXISTS + " " + trainNumber);
        }

        doGet(req, resp);


    }
}
