package com.Alchuk.controller.admin;


import com.Alchuk.model.defaultMessages.Messages;
import com.Alchuk.model.entities.route.Route;
import com.Alchuk.model.entities.train.Train;
import com.Alchuk.model.errorMessages.ErrorMessages;
import com.Alchuk.model.exceptions.InvalidInputException;
import com.Alchuk.model.exceptions.SuchTrainDoesntExistOrIsUnavailable;
import com.Alchuk.model.injecotrs.TrainServiceInjector;
import com.Alchuk.model.service.RouteService;
import com.Alchuk.model.injecotrs.RouteServiceInjector;
import com.Alchuk.model.service.TrainService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static com.Alchuk.model.fieldChecker.RouteFieldChecker.checkInputFields;


public class RouteController extends HttpServlet {

    private static final Logger logger = LogManager.getLogger(RouteController.class);
    private final RouteService routeService;
    private final TrainService trainService;

    public RouteController() {
        routeService = RouteServiceInjector.getRouteServiceInjector().inject();
        trainService = TrainServiceInjector.getTrainServiceInjector().inject();
    }


    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("JSP/Route.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String routeId = req.getParameter("routeId");
        String fromCity = req.getParameter("fromCity");
        String toCity = req.getParameter("toCity");
        String departureTime = req.getParameter("departureTime");
        String trainNumber = req.getParameter("trainNumber");
        String distance = req.getParameter("distance");

        try {
            checkInputFields(routeId, fromCity, toCity, departureTime, distance);
            if (!routeService.findRouteByID(Long.parseLong(routeId))) {
                Train trainOnThisRoute = trainService.findTrainByNumber(trainNumber);
                if (trainOnThisRoute == null || !trainOnThisRoute.isAvailable()) {
                    throw new SuchTrainDoesntExistOrIsUnavailable(ErrorMessages.TRAIN_DOESNT_EXIST_OR_UNAVAILABLE.getMessage());
                }
                trainService.updateAvailability(trainNumber);
                Route routeToAdd = routeService.createRoute(routeId, fromCity, toCity, departureTime, distance, trainOnThisRoute);
                routeService.addRoute(routeToAdd);
                  logger.info(Messages.ROUTE_ADDED + " " + routeId);
                req.setAttribute("info", Messages.ROUTE_ADDED.getMessage());
            } else {
                req.setAttribute("error", ErrorMessages.ROUTE_ALREADY_EXISTS.getMessage());
                logger.error(ErrorMessages.ROUTE_ALREADY_EXISTS.getMessage() + " " + routeId);
            }
        } catch (InvalidInputException e) {
            req.setAttribute("error", e.getMassage());
            logger.error("Invalid Input:" + " " + e.getMassage());
        } catch (SQLException | ClassNotFoundException e) {
            req.setAttribute("error", ErrorMessages.DEFAULT_EXCEPTION.getMessage());
            logger.error("Inner fail" + " " + e.getMessage());

        } catch (SuchTrainDoesntExistOrIsUnavailable e) {
            logger.error(trainNumber + " " + e.getMessage());
            req.setAttribute("error", e.getMessage());
        }
        doGet(req, resp);

    }
}
