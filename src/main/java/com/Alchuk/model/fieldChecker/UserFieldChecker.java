package com.Alchuk.model.fieldChecker;

import com.Alchuk.model.errorMessages.ErrorMessages;
import com.Alchuk.model.exceptions.InvalidInputException;

public class UserFieldChecker {


    public static void checkInputFields(String userName, String password, String firstName, String lastName, String telephoneNumber) throws InvalidInputException {
        checkUserName(userName);
        checkUserPassword(password);
        checkUserFirstNameAndLastName(firstName, lastName);
        checkUserTelephoneNumber(telephoneNumber);
    }

    public static void checkUserName(String userName) throws InvalidInputException {

        if (userName.length() <= 3 || userName.length() >= 15) {
            throw new InvalidInputException(ErrorMessages.INCORRECT_USER_NAME.getMessage());

        }
    }

    public static void checkUserPassword(String password) throws InvalidInputException {
        if (password.length() >= 200 || password.length() <= 7) {
            throw new InvalidInputException(ErrorMessages.INCORRECT_PASS.getMessage());
        }

    }

    public static void checkUserFirstNameAndLastName(String firstName, String lastName) throws InvalidInputException {
        if (!firstName.matches("^[a-zA-Z]*$") || !lastName.matches("^[a-zA-Z]*$")) {
            throw new InvalidInputException(ErrorMessages.INCORRECT_INITIALS.getMessage());

        }

    }

    public static void checkUserTelephoneNumber(String telephoneNumber) throws InvalidInputException {

        if (!telephoneNumber.matches("[0-9]+")) {
            throw new InvalidInputException(ErrorMessages.INCORRECT_NUMBER.getMessage());

        }

    }
}
