package com.Alchuk.model.fieldChecker;

import com.Alchuk.model.errorMessages.ErrorMessages;
import com.Alchuk.model.exceptions.InvalidInputException;
public class RouteFieldChecker {

    public static void checkInputFields(String routeId, String fromCity, String toCity, String departureTime, String distance) throws InvalidInputException{

        if(!routeId.matches("[\\d]+") || !distance.matches("[\\d]+") || !departureTime.matches("[\\d]{1,2}")){
            throw new InvalidInputException(ErrorMessages.INCORRECT_ROUTE_INFO_INPUT.getMessage());
        }
        if(!fromCity.matches("[\\D]+") || !toCity.matches("[\\D]+")){
            throw new InvalidInputException(ErrorMessages.INCORRECT_ROUTE_INFO_INPUT.getMessage());
        }
        if(fromCity.trim().equals(toCity.trim())){
            throw new InvalidInputException(ErrorMessages.INCORRECT_ROUTE_INFO_INPUT.getMessage());
        }
    }

}
