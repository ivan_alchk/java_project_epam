package com.Alchuk.model.fieldChecker;

import com.Alchuk.model.errorMessages.ErrorMessages;
import com.Alchuk.model.exceptions.InvalidInputException;

public class TrainFieldChecker {

    public static void  checkInputFields(String  trainNumber, String trainType) throws InvalidInputException {

        if(!trainType.equals("GT") && !trainType.equals("S")){
            throw new InvalidInputException(ErrorMessages.INCORRECT_TYPE_OF_TRAIN.getMessage());
        }
        if(!trainNumber.startsWith("S") && !trainNumber.startsWith("GT") && !trainNumber.substring(1).matches("\\d{6}")){
            throw new InvalidInputException(ErrorMessages.INCORRECT_TRAIN_NUMBER.getMessage());
        }
        if(trainNumber.charAt(0) != trainType.charAt(0)){
            throw  new InvalidInputException(ErrorMessages.TRAIN_TYPE_NOT_CORRESPONDS_WITH_TRAIN_TYPE.getMessage());
        }


    }
}
