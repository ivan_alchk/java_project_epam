package com.Alchuk.model.defaultMessages;

public enum Messages {


    USER_ADDED("Successfully registered"),
    ROUTE_ADDED("Route successfully added"),
    TRAIN_ADDED("Train successfully added"),
    USER_DELETED("Successfully deleted"),
    USER_NAME_CHANGED("UserName successfully changed to"),
    PASSWORD_CHANGED("Successfully changed password"),
    ROUTE_DELETED("Successfully deleted route number");


    private final String message;


    Messages(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
