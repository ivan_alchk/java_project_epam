package com.Alchuk.model.trainFactory;

import com.Alchuk.model.entities.train.GeneralTrain;
import com.Alchuk.model.entities.train.Shinkansen;
import com.Alchuk.model.entities.train.Train;

public class TrainFactory {

    private static final TrainFactory trainFactory = new TrainFactory();

    private TrainFactory(){

    }

    public Train createTrainByTrainNumber(String trainNumber){
        String type = getTrainTypeFromNumber(trainNumber);
        return type.equals("S") ? new Shinkansen(trainNumber) : new GeneralTrain(trainNumber);

    }
    public Train createTrainByType(String trainType, String trainNumber){
        return trainType.equals("S") ? new Shinkansen(trainNumber) : new GeneralTrain(trainNumber);
    }

    public static TrainFactory getTrainFactory(){
        return trainFactory;
    }

    private String getTrainTypeFromNumber(String trainNumber){
        if(trainNumber.charAt(0) == 'S'){
            return "S";
        }
        else{
            return "GT";
        }
    }
}

