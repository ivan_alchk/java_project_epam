package com.Alchuk.model.injecotrs;

import com.Alchuk.model.dataLayer.daoImpl.TrainDaoImpl;
import com.Alchuk.model.service.RouteService;
import com.Alchuk.model.dataLayer.daoImpl.RouteDaoImpl;
import com.Alchuk.model.service.RouteServiceImpl;

import java.sql.SQLException;

public class RouteServiceInjector {

    private static final RouteServiceInjector routeServiceInjector = new RouteServiceInjector();
    private static  RouteService routeService;

    static {
        try {
            routeService = new RouteServiceImpl(new RouteDaoImpl());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    private RouteServiceInjector(){

    }

    public static RouteServiceInjector getRouteServiceInjector() {
        return routeServiceInjector;
    }

    public RouteService inject() {
        return routeService;

    }
}
