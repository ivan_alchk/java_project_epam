package com.Alchuk.model.injecotrs;

import com.Alchuk.model.dataLayer.daoImpl.TrainDaoImpl;
import com.Alchuk.model.service.TrainService;
import com.Alchuk.model.service.TrainServiceImpl;

import java.sql.SQLException;

public class TrainServiceInjector {

    private static final TrainServiceInjector trainServiceInjector = new TrainServiceInjector();

    private static  TrainService trainService;

    static {
        try {
            trainService = new TrainServiceImpl(new TrainDaoImpl());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private TrainServiceInjector() {

    }

    public TrainService inject() {
        return trainService;
    }

    public static TrainServiceInjector getTrainServiceInjector() {
        return trainServiceInjector;
    }

}
