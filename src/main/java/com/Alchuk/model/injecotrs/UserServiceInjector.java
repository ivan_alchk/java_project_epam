package com.Alchuk.model.injecotrs;


import com.Alchuk.model.dataLayer.daoImpl.UserDaoImpl;
import com.Alchuk.model.service.UserService;
import com.Alchuk.model.service.UserServiceImpl;

import java.sql.SQLException;

public class UserServiceInjector {
    private final static UserServiceInjector userServiceInjector = new UserServiceInjector();
    private static UserService userService;

    static {
        try {
            userService = new UserServiceImpl(new UserDaoImpl());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private UserServiceInjector() {

    }

    public UserService inject() {
        return userService;
    }


    public static UserServiceInjector getUserServiceInjector() {
        return userServiceInjector;
    }
}
