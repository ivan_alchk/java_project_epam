package com.Alchuk.model.entities.train;

import java.util.Objects;

public class GeneralTrain implements Train {


    private final static int speedPerHour = 100;
    private final static int amountOfPlaces = 300;
    private final String trainNumber;
    private final static String TRAIN_TYPE = "GT";
    private boolean availability = true;

    public GeneralTrain(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public String getTrainType() {
        return TRAIN_TYPE;
    }

    public boolean isAvailable() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    @Override
    public int calculateWayTime(int distance) {
        return distance / speedPerHour;
    }

    public int getSpeedPerHour() {
        return speedPerHour;
    }

    public int getAmountOfPlaces() {
        return amountOfPlaces;
    }

    @Override
    public String toString() {
        return "GeneralTrain" + " " +
                "trainNumber='" + trainNumber + '\'' +
                ", availability=" + availability;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GeneralTrain that = (GeneralTrain) o;
        return trainNumber.equals(that.trainNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trainNumber);
    }
}


