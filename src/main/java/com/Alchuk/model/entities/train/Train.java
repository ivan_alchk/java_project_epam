package com.Alchuk.model.entities.train;

public interface Train {

    int calculateWayTime(int distance);
    int getSpeedPerHour();
    int getAmountOfPlaces();
    String getTrainType();
    String getTrainNumber();
    boolean isAvailable();
    void setAvailability(boolean availability);
    String toString();
}
