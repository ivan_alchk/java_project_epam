package com.Alchuk.model.entities.train;

import java.util.Objects;

public class Shinkansen implements Train {


    private final static int speedPerHour = 300;
    private final static int amountOfPlaces = 200;
    private final String trainNumber;
    private final static String trainType = "S";
    private boolean availability = true;

    public boolean isAvailable() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    @Override
    public String getTrainNumber() {
        return trainNumber;
    }

    @Override
    public  String getTrainType() {
        return trainType;
    }

    public Shinkansen(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    @Override
    public int calculateWayTime(int distance) {
        return distance/speedPerHour;
    }

    public  int getSpeedPerHour() {
        return speedPerHour;
    }

    public  int getAmountOfPlaces() {
        return amountOfPlaces;
    }


    @Override
    public String toString() {
        return "Shinkansen" + " " +
                "trainNumber='" + trainNumber + '\'' +
                ", availability=" + availability;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shinkansen that = (Shinkansen) o;
        return trainNumber.equals(that.trainNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trainNumber);
    }
}
