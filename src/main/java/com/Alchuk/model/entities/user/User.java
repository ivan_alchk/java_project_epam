package com.Alchuk.model.entities.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class User {


    private final String userName;
    private String password;
    private final String firstName;
    private final String lastName;
    private final String telephoneNumber;
    private final List<Long> routesId;

    public User(String userName, String password, String firstName, String lastName, String telephoneNumber) {
        this.userName = userName;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.telephoneNumber = telephoneNumber;
        this.routesId = new ArrayList<>();
    }

    public String getUserName() {
        return userName;
    }


    public String getPassword() {
        return password;

    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public List<Long> getRoutesId() {
        return routesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return userName.equals(user.userName) &&
                password.equals(user.password) &&
                firstName.equals(user.firstName) &&
                lastName.equals(user.lastName) &&
                telephoneNumber.equals(user.telephoneNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, password, firstName, lastName, telephoneNumber);
    }
}
