package com.Alchuk.model.entities.route;

import com.Alchuk.model.entities.train.Train;

import java.util.Objects;

public class Route  {

    private long id;
    private String from;
    private String to;
    private int distance;
    private final String departureTime;
    private final Train trainOnThisRoute;
    private String arrivalTime;

    public Route(long id, String from, String to, int distance, String departureTime, Train trainOnThisRoute) {
        this.from = from;
        this.id = id;
        this.to = to;
        this.distance = distance;
        this.departureTime = departureTime;
        this.trainOnThisRoute = trainOnThisRoute;
        trainOnThisRoute.setAvailability(false);


    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getDepartureTime() {
        return departureTime;
    }


    public void calculateArrivalTime() throws ClassCastException {
        int tempArrivalTime = (trainOnThisRoute.calculateWayTime(distance) + Integer.parseInt(this.departureTime));
        this.arrivalTime = Integer.toString(tempArrivalTime);
    }

    public String getArrivalTime (){
        return arrivalTime;
    }

    public Train getTrainOnThisRoute() {
        return trainOnThisRoute;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return String.format("Route number:%d  %s-%s  departure:%s arrival:%s train number:%s", id, from, to, departureTime, arrivalTime, trainOnThisRoute.getTrainNumber());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Route route = (Route) o;
        return id == route.id &&
                distance == route.distance &&
                from.equals(route.from) &&
                to.equals(route.to) &&
                departureTime.equals(route.departureTime) &&
                trainOnThisRoute.equals(route.trainOnThisRoute);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, from, to, distance, departureTime, trainOnThisRoute);
    }
}
