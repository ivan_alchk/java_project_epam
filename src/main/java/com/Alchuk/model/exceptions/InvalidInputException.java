package com.Alchuk.model.exceptions;

public class InvalidInputException extends RuntimeException {

    private final String errorMessage;
    public InvalidInputException(String errorMassage){
        this.errorMessage = errorMassage;
    }

    public String getMassage() {
        return errorMessage;
    }
}
