package com.Alchuk.model.exceptions;

public class UserAlreadyExistsException extends RuntimeException {
    private final String errorMessage;

    public UserAlreadyExistsException(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getMessage() {
        return errorMessage;
    }
}
