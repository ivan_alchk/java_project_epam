package com.Alchuk.model.exceptions;

public class TrainWithSuchNumberAlreadyExistsException extends RuntimeException{

    private final String message;

    public TrainWithSuchNumberAlreadyExistsException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
