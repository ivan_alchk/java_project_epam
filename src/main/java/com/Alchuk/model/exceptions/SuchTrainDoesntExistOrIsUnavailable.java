package com.Alchuk.model.exceptions;

public class SuchTrainDoesntExistOrIsUnavailable extends RuntimeException {
    private final String message;

    public SuchTrainDoesntExistOrIsUnavailable(String message){
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
