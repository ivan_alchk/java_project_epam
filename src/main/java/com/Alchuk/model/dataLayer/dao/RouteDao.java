package com.Alchuk.model.dataLayer.dao;

import com.Alchuk.model.entities.route.Route;

import java.sql.SQLException;
import java.util.List;

public interface RouteDao {


    void addRoute(Route route) throws ClassNotFoundException, SQLException;
    boolean findRouteByID(long id) throws ClassNotFoundException, SQLException;

    List<Route> getAllRoutes() throws ClassNotFoundException, SQLException;
    void clearAllDataFromTable() throws SQLException, ClassNotFoundException;



}
