package com.Alchuk.model.dataLayer.dao;

import com.Alchuk.model.entities.user.User;
import com.Alchuk.model.exceptions.UserAlreadyExistsException;

import java.sql.SQLException;
import java.util.List;

public interface UserDao {

    void addUser(User user) throws SQLException, ClassNotFoundException;

    boolean findUserByUserName(String userName) throws SQLException, ClassNotFoundException;

    void deleteByUserName(String userName) throws ClassNotFoundException, SQLException;

    void updateUserName(String newUserName, String oldUserName) throws ClassNotFoundException, SQLException;

    void updatePassword(String userName, String newPassword) throws ClassNotFoundException, SQLException;

    void setRouteToUser(int routeID, String userName) throws ClassNotFoundException, SQLException;

    List<Integer> getAllUserRoutes(String userName) throws ClassNotFoundException, SQLException;

    void deleteUserRoute(String userName, Integer routeId) throws ClassNotFoundException, SQLException;


    boolean findUserByUserNameAndPassword(String userName, String password) throws SQLException, ClassNotFoundException;

    void clearAllDataFromTable() throws SQLException, ClassNotFoundException;
}
