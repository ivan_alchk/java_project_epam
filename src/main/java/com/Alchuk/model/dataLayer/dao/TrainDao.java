package com.Alchuk.model.dataLayer.dao;

import com.Alchuk.model.entities.train.Train;

import java.sql.SQLException;
import java.util.List;

public interface TrainDao {

    void addTrain(Train train) throws SQLException, ClassNotFoundException;

    Train findTrainByNumber(String number) throws SQLException, ClassNotFoundException;

    boolean checkIfTrainAvailable(String trainNumber) throws SQLException, ClassNotFoundException;

    List<Train> getAll() throws SQLException, ClassNotFoundException;

    void updateAvailability(String trainNumber) throws ClassNotFoundException, SQLException;
    void clearAllDataFromTable() throws SQLException, ClassNotFoundException;
}
