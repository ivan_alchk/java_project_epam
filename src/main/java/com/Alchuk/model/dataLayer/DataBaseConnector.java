package com.Alchuk.model.dataLayer;

import com.Alchuk.model.Constants;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseConnector {


    private  Connection connection;
    private static final DataBaseConnector DATA_BASE_CONNECTION = new DataBaseConnector();

    private DataBaseConnector(){

    }

    public static DataBaseConnector getDataBaseConnector() {
        return DATA_BASE_CONNECTION;
    }


    public  Connection getConnection() throws ClassNotFoundException, SQLException {
        if (connection == null) {
            Class.forName(Constants.DRIVER);
           return connection = DriverManager.getConnection(Constants.DB_URL, "sa", "");
        }
        return connection;

    }






}
