package com.Alchuk.model.dataLayer.daoImpl;

import com.Alchuk.model.dataLayer.DataBaseConnector;
import com.Alchuk.model.dataLayer.dao.RouteDao;
import com.Alchuk.model.entities.route.Route;
import com.Alchuk.model.entities.train.Train;
import com.Alchuk.model.trainFactory.TrainFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RouteDaoImpl implements RouteDao {


    private Connection connection;
    private Statement statement;


    public RouteDaoImpl() throws SQLException, ClassNotFoundException {
        createRouteTable();
    }


    @Override
    public void addRoute(Route route) throws ClassNotFoundException, SQLException {

        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        Statement newRouteStatement = connection.createStatement();
        String sql = String.format("INSERT INTO ROUTES" + " " + "values('%s', '%s', '%s', '%s', '%s', '%s', '%d')", route.getId(), route.getFrom(), route.getTo(), route.getDepartureTime(), route.getArrivalTime(), route.getTrainOnThisRoute().getTrainNumber(), route.getDistance());

        newRouteStatement.executeUpdate(sql);

    }

    @Override
    public boolean findRouteByID(long id) throws ClassNotFoundException, SQLException {

            connection = DataBaseConnector.getDataBaseConnector().getConnection();
            statement = connection.createStatement();
            String sql = String.format("SELECT * FROM ROUTES WHERE ROUTE_ID = '%s'", id);
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                long tempId = rs.getLong("ROUTE_ID");
                if (tempId == id) {
                    return true;
                }

            }




        return false;
    }

    @Override
    public List<Route> getAllRoutes() throws ClassNotFoundException, SQLException {
        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        statement = connection.createStatement();
        String sql = "SELECT * FROM ROUTES";
        ResultSet rs = statement.executeQuery(sql);
        List<Route> routeList = new ArrayList<>();
        while (rs.next()) {
            String tempFromCity = rs.getString("FROM_CITY");
            String tempToCity = rs.getString("TO_CITY");
            String tempRouteID = rs.getString("ROUTE_ID");
            String tempDepartureTime = rs.getString("DEPARTURE_TIME");
            String tempArrivalTime = rs.getString("ARRIVAL_TIME");
            int tempDistance = rs.getInt("DISTANCE");
            Train tempTrain = TrainFactory.getTrainFactory().createTrainByTrainNumber(rs.getString("TRAIN_NUMBER"));
            Route tempRoute = new Route(Long.parseLong(tempRouteID), tempFromCity, tempToCity, tempDistance, tempDepartureTime, tempTrain);
            tempRoute.setArrivalTime(tempArrivalTime);
            routeList.add(tempRoute);
        }
        return routeList;
    }

    private void createRouteTable() throws SQLException, ClassNotFoundException {
        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        statement = connection.createStatement();
        String sql = "CREATE TABLE IF NOT EXISTS ROUTES" +
                "(ROUTE_ID VARCHAR(10)," +
                "FROM_CITY VARCHAR(50)," +
                "TO_CITY VARCHAR (50)," +
                "DEPARTURE_TIME VARCHAR (10)," +
                "ARRIVAL_TIME VARCHAR (10), " +
                "TRAIN_NUMBER VARCHAR (10)," +
                "DISTANCE INTEGER)";
        statement.executeUpdate(sql);

    }

    @Override
    public void clearAllDataFromTable() throws SQLException, ClassNotFoundException {
        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        statement = connection.createStatement();
        String sql = "DELETE FROM ROUTES";
        statement.executeUpdate(sql);
    }
}
