package com.Alchuk.model.dataLayer.daoImpl;


import com.Alchuk.model.dataLayer.DataBaseConnector;
import com.Alchuk.model.dataLayer.dao.UserDao;

import com.Alchuk.model.entities.user.User;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;



public class UserDaoImpl implements UserDao {
    private Connection connection;
    private Statement statement;


    public UserDaoImpl() throws SQLException, ClassNotFoundException {
        createUserTable();
    }

    @Override
    public void addUser(User user) throws SQLException, ClassNotFoundException {


        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        String userInfo = String.format("INSERT INTO USERS" + " " + "values('%s', '%s', '%s', '%s', '%s', ?)", user.getUserName(), user.getPassword(), user.getFirstName(), user.getLastName(), user.getTelephoneNumber());

        PreparedStatement preparedStatement = connection.prepareStatement(userInfo);
        preparedStatement.setArray(1, connection.createArrayOf("INTEGER", new Integer[0]));
        preparedStatement.executeUpdate();


    }

    @Override
    public boolean findUserByUserName(String userName) throws ClassNotFoundException, SQLException {

        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        statement = connection.createStatement();
        String sql = String.format("SELECT * FROM USERS WHERE USER_NAME = '%s'", userName);
        ResultSet rs = statement.executeQuery(sql);
        while (rs.next()) {
            String tempUser = rs.getString("USER_NAME");
            if (userName.equals(tempUser)) {
                return true;
            }
        }


        return false;
    }

    @Override
    public boolean findUserByUserNameAndPassword(String userName, String password) throws SQLException, ClassNotFoundException {
        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        statement = connection.createStatement();
        String sql = String.format("SELECT * FROM USERS WHERE USER_NAME = '%s' AND PASSWORD = '%s'", userName, password);
        ResultSet rs = statement.executeQuery(sql);
        while (rs.next()) {
            String tempUser = rs.getString("USER_NAME");
            String tempPassword = rs.getString("PASSWORD");
            if (userName.equals(tempUser) && password.equals(tempPassword)) {
                return true;
            }
        }


        return false;
    }

    @Override
    public void deleteByUserName(String userName) throws ClassNotFoundException, SQLException {
        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        statement = connection.createStatement();
        String sql = String.format("DELETE FROM USERS WHERE USER_NAME = '%s';", userName);
        statement.executeUpdate(sql);
    }

    @Override
    public void updateUserName(String newUserName, String oldUserName) throws ClassNotFoundException, SQLException {
        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        statement = connection.createStatement();
        String sql = String.format("UPDATE USERS SET USER_NAME = '%s' WHERE USER_NAME = '%s'", newUserName, oldUserName);
        statement.executeUpdate(sql);

    }

    @Override
    public void updatePassword(String userName, String newPassword) throws ClassNotFoundException, SQLException {
        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        statement = connection.createStatement();
        String sql = String.format("UPDATE USERS SET PASSWORD = '%s' WHERE USER_NAME = '%s'", newPassword, userName);
        statement.executeUpdate(sql);
    }

    @Override
    public void setRouteToUser(int routeID, String userName) throws ClassNotFoundException, SQLException {

        List<Integer> listToUpdate = getAllUserRoutes(userName);
        listToUpdate.add(routeID);
        updateUserRoutes(listToUpdate, userName);



    }

    @Override
    public List<Integer> getAllUserRoutes(String userName) throws ClassNotFoundException, SQLException {
        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        statement = connection.createStatement();

        String sql = String.format("SELECT * FROM USERS WHERE USER_NAME = '%s'", userName);
        ResultSet rs = statement.executeQuery(sql);


        List<Integer> routes = new ArrayList<>();
        while (rs.next()) {
           Object[] temp = (Object[])rs.getArray("ROUTES").getArray();
           for(Object object : temp){
               routes.add((Integer)object);
           }
        }

        return routes;


    }

    @Override
    public void deleteUserRoute(String userName, Integer routeId) throws ClassNotFoundException, SQLException {

        List<Integer> userRoutes = getAllUserRoutes(userName);
        userRoutes.remove(routeId);
        updateUserRoutes(userRoutes, userName);


    }


    private void updateUserRoutes(List<Integer> routesID, String userName) throws ClassNotFoundException, SQLException{
        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(String.format("UPDATE USERS SET ROUTES = ? WHERE USER_NAME = '%s' ", userName));
        preparedStatement.setArray(1, connection.createArrayOf("INTEGER", routesID.toArray()));
        preparedStatement.executeUpdate();

    }

    private void createUserTable() throws ClassNotFoundException, SQLException {
        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        statement = connection.createStatement();
        String sql = "CREATE TABLE IF NOT EXISTS USERS " +
                "(USER_NAME VARCHAR(255), " +
                " PASSWORD VARCHAR(255), " +
                " FIRST_NAME VARCHAR(255), " +
                " LAST_NAME VARCHAR(255), " +
                " TELEPHONE VARCHAR(255)," +
                "ROUTES  ARRAY[10]) ";
        statement.executeUpdate(sql);

    }
    @Override
    public void clearAllDataFromTable() throws SQLException, ClassNotFoundException {
        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        statement = connection.createStatement();
        String sql = "DELETE FROM USERS";
        statement.executeUpdate(sql);
    }
}


