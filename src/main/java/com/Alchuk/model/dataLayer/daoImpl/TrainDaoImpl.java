package com.Alchuk.model.dataLayer.daoImpl;

import com.Alchuk.model.Constants;

import com.Alchuk.model.dataLayer.DataBaseConnector;
import com.Alchuk.model.dataLayer.dao.TrainDao;
import com.Alchuk.model.entities.train.Train;
import com.Alchuk.model.trainFactory.TrainFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TrainDaoImpl implements TrainDao {
    private Connection connection;
    private Statement statement;


    public TrainDaoImpl() throws SQLException, ClassNotFoundException {
        createTrainTable();
    }


    @Override
    public void addTrain(Train train) throws SQLException, ClassNotFoundException {

        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        Statement newTrain = connection.createStatement();
        String sql = String.format("INSERT INTO TRAINS" + " " + "values('%s', '%s', '%d', '%b', '%d')", train.getTrainNumber(), train.getTrainType(), train.getAmountOfPlaces(), train.isAvailable(), train.getSpeedPerHour());
        newTrain.executeUpdate(sql);


    }

    @Override
    public boolean checkIfTrainAvailable(String trainNumber) throws SQLException, ClassNotFoundException {


        connection = DataBaseConnector.getDataBaseConnector().getConnection();


        statement = connection.createStatement();
        String sql = String.format("SELECT * FROM TRAINS WHERE TRAIN_NUMBER = '%s'", trainNumber);
        ResultSet rs = statement.executeQuery(sql);
        while (rs.next()) {

            boolean tempAvailability = rs.getBoolean("AVAILABILITY");
            String tempNumber = rs.getString("TRAIN_NUMBER");
            if (tempNumber.equals(trainNumber) && tempAvailability) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Train findTrainByNumber(String number) throws ClassNotFoundException, SQLException {

        connection = DataBaseConnector.getDataBaseConnector().getConnection();

        connection = DriverManager.getConnection(Constants.DB_URL, "sa", "");
        statement = connection.createStatement();
        String sql = String.format("SELECT * FROM TRAINS WHERE TRAIN_NUMBER = '%s'", number);
        ResultSet rs = statement.executeQuery(sql);
        while (rs.next()) {
            String tempNumber = rs.getString("TRAIN_NUMBER");
            if (number.equals(tempNumber)) {
                Train train = TrainFactory.getTrainFactory().createTrainByTrainNumber(number);
                train.setAvailability(rs.getBoolean("AVAILABILITY"));
                return train;

            }
        }
        return null;


    }

    @Override
    public List<Train> getAll() throws SQLException, ClassNotFoundException {
        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        statement = connection.createStatement();
        String sql = "SELECT * FROM TRAINS";
        ResultSet rs = statement.executeQuery(sql);
        List<Train> trains = new ArrayList<>();
        while (rs.next()) {
           Train trainToAdd = TrainFactory.getTrainFactory().createTrainByType(rs.getString("TRAIN_TYPE"), rs.getString("TRAIN_NUMBER"));
           trainToAdd.setAvailability(rs.getBoolean("AVAILABILITY"));
           trains.add(trainToAdd);

        }

        return trains;
    }

    @Override
    public void updateAvailability(String trainNumber) throws ClassNotFoundException, SQLException {
        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        statement = connection.createStatement();
        String sql = String.format("UPDATE TRAINS SET AVAILABILITY = '%b' WHERE TRAIN_NUMBER = '%s'", false, trainNumber);
        statement.executeUpdate(sql);
    }

    private void createTrainTable() throws SQLException, ClassNotFoundException {
        connection = DataBaseConnector.getDataBaseConnector().getConnection();


        statement = connection.createStatement();
        String sql = "CREATE TABLE IF NOT EXISTS TRAINS" +
                "(TRAIN_NUMBER VARCHAR(10)," +
                "TRAIN_TYPE VARCHAR(10)," +
                "AMOUNT_OF_PLACES INTEGER (10)," +
                "AVAILABILITY BIT(1)," +
                "SPEED_PER_HOUR INTEGER (10) )";
        statement.executeUpdate(sql);

    }

    @Override
    public void clearAllDataFromTable() throws SQLException, ClassNotFoundException {
        connection = DataBaseConnector.getDataBaseConnector().getConnection();
        statement = connection.createStatement();
        String sql = "DELETE FROM TRAINS";
        statement.executeUpdate(sql);
    }
}
