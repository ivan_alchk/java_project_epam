package com.Alchuk.model.service;

import com.Alchuk.model.entities.train.Train;

import java.sql.SQLException;
import java.util.List;

public interface TrainService {
    void addTrain(Train train) throws SQLException, ClassNotFoundException;


    Train findTrainByNumber(String trainNumber) throws SQLException, ClassNotFoundException;

    List<Train> getAll() throws SQLException, ClassNotFoundException;

    Train createTrain(String trainNumber, String trainType);

    void updateAvailability(String trainNumber) throws SQLException, ClassNotFoundException;
}
