package com.Alchuk.model.service;

import com.Alchuk.model.dataLayer.dao.UserDao;
import com.Alchuk.model.entities.user.User;
import com.Alchuk.model.exceptions.UserAlreadyExistsException;

import java.sql.SQLException;
import java.util.List;

public class UserServiceImpl implements UserService {

    private final UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public  void addUser(User user) throws SQLException, ClassNotFoundException {
        this.userDao.addUser(user);
    }

    @Override
    public User createUser(String userName, String password, String firstName, String lastName, String telephoneNumber) {
            return new User(userName, password, firstName, lastName, telephoneNumber);
    }
    @Override
    public boolean findUserByUserName(String userName) throws ClassNotFoundException, SQLException {
        return userDao.findUserByUserName(userName);
    }

    @Override
    public boolean findUserByUserNameAndPassword(String userName, String password) throws ClassNotFoundException, SQLException {
        return userDao.findUserByUserNameAndPassword(userName, password);
    }

    @Override
    public void deleteByUserName(String userName) throws ClassNotFoundException, SQLException {
        userDao.deleteByUserName(userName);
    }

    @Override
    public void updateUserName(String newUserName, String oldUserName) throws ClassNotFoundException, SQLException {
        userDao.updateUserName(newUserName, oldUserName);

    }

    @Override
    public void updatePassword(String userName, String newPassword) throws ClassNotFoundException, SQLException {
        userDao.updatePassword(userName, newPassword);

    }

    @Override
    public List<Integer> getAllUserRoutes(String userName) throws ClassNotFoundException, SQLException {
        return userDao.getAllUserRoutes(userName);
    }

    @Override
    public void setRouteToUser(int routeID, String userName) throws ClassNotFoundException, SQLException {
        userDao.setRouteToUser(routeID, userName);

    }

    @Override
    public void deleteUserRoute(int routeId, String userName) throws ClassNotFoundException, SQLException {
        userDao.deleteUserRoute(userName, routeId);
    }
}
