package com.Alchuk.model.service;

import com.Alchuk.model.dataLayer.dao.RouteDao;
import com.Alchuk.model.dataLayer.dao.TrainDao;
import com.Alchuk.model.entities.route.Route;
import com.Alchuk.model.entities.train.Train;
import com.Alchuk.model.errorMessages.ErrorMessages;
import com.Alchuk.model.exceptions.SuchTrainDoesntExistOrIsUnavailable;
import com.Alchuk.model.trainFactory.TrainFactory;

import java.sql.SQLException;
import java.util.List;

public class RouteServiceImpl implements RouteService {


    private final RouteDao routeDao;


    public RouteServiceImpl(RouteDao routDao) {
        this.routeDao = routDao;

    }

    @Override
    public void addRoute(Route route) throws SQLException, ClassNotFoundException {

        routeDao.addRoute(route);


    }

    @Override
    public boolean findRouteByID(long id) throws SQLException, ClassNotFoundException {
        return routeDao.findRouteByID(id);
    }

    @Override
    public List<Route> getAllRoutes() throws SQLException, ClassNotFoundException {
        return routeDao.getAllRoutes();
    }

    @Override
    public Route createRoute(String routeID, String fromCity, String toCity, String departureTime, String distance, Train train) {
        Route routeToReturn = new Route(Long.parseLong(routeID), fromCity, toCity,Integer.parseInt(distance), departureTime, train);
        routeToReturn.calculateArrivalTime();
        return routeToReturn;
    }
}
