package com.Alchuk.model.service;

import com.Alchuk.model.entities.route.Route;
import com.Alchuk.model.entities.train.Train;
import com.Alchuk.model.exceptions.SuchTrainDoesntExistOrIsUnavailable;

import java.sql.SQLException;
import java.util.List;

public interface RouteService {

    void addRoute(Route route) throws SQLException, ClassNotFoundException;

    boolean findRouteByID(long id) throws SQLException, ClassNotFoundException;

    List<Route> getAllRoutes() throws SQLException, ClassNotFoundException;

    Route createRoute(String routeID, String fromCity, String toCity, String departureTime, String distance, Train train);
}

