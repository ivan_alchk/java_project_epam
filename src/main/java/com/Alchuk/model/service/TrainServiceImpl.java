package com.Alchuk.model.service;

import com.Alchuk.model.dataLayer.dao.TrainDao;
import com.Alchuk.model.entities.train.Train;
import com.Alchuk.model.trainFactory.TrainFactory;

import java.sql.SQLException;
import java.util.List;


public class TrainServiceImpl implements TrainService {


    private final TrainDao trainDao;


    public TrainServiceImpl(TrainDao trainDao) {
        this.trainDao = trainDao;
    }

    @Override
    public void addTrain(Train train) throws SQLException, ClassNotFoundException {

        trainDao.addTrain(train);
    }



    @Override
    public Train findTrainByNumber(String trainNumber) throws SQLException, ClassNotFoundException {
        return trainDao.findTrainByNumber(trainNumber);
    }
    @Override
    public List<Train> getAll() throws SQLException, ClassNotFoundException{
        return trainDao.getAll();
    }

    @Override
    public Train createTrain(String trainNumber, String trainType) {
        return TrainFactory.getTrainFactory().createTrainByType(trainType, trainNumber);
    }

    @Override
    public void updateAvailability(String trainNumber) throws SQLException, ClassNotFoundException {
        trainDao.updateAvailability(trainNumber);
    }
}
