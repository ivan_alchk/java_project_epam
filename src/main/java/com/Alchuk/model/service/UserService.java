package com.Alchuk.model.service;

import com.Alchuk.model.entities.user.User;
import com.Alchuk.model.exceptions.UserAlreadyExistsException;
import org.h2.mode.FunctionsMSSQLServer;

import java.sql.SQLException;
import java.util.List;

public interface UserService {
    void addUser(User user) throws SQLException, ClassNotFoundException;

    boolean findUserByUserName(String userName) throws ClassNotFoundException, SQLException;

    void deleteByUserName(String userName) throws ClassNotFoundException, SQLException;

    void updateUserName(String newUserName, String oldUserName) throws ClassNotFoundException, SQLException;

    void updatePassword(String userName, String newPassword) throws ClassNotFoundException, SQLException;

    User createUser(String userName, String password, String firstName, String lastName, String telephoneNumber);

    List<Integer> getAllUserRoutes(String userName) throws ClassNotFoundException, SQLException;

    void setRouteToUser(int routeID, String userName) throws ClassNotFoundException, SQLException;

    void deleteUserRoute(int routeId, String userName) throws ClassNotFoundException, SQLException;

    boolean findUserByUserNameAndPassword(String userName, String password) throws ClassNotFoundException, SQLException;
}
