package com.Alchuk.model.errorMessages;

import com.Alchuk.model.defaultMessages.Messages;

public enum ErrorMessages {

    INCORRECT_USER_NAME("Invalid name input! User name must have 4 to 15 characters!"),
    INCORRECT_PASS("Invalid password input! Password must have at least 8 characters "),
    INCORRECT_NUMBER("Invalid telephone number input! Only numbers are allowed"),
    INCORRECT_INITIALS("Invalid input of first or last name! No numbers are allowed! "),
    DEFAULT_EXCEPTION("Something went wrong"),
    USER_EXISTS("User already exists, please log in!"),
    INCORRECT_INFO("Incorrect login or password"),
    TRAIN_EXISTS("Such train already exists"),
    TRAIN_DOESNT_EXIST_OR_UNAVAILABLE("Such train does not exist or unavailable"),
    INCORRECT_TYPE_OF_TRAIN("Incorrect train type, it must be 'S' or 'GT'"),
    INCORRECT_TRAIN_NUMBER("Train number must start with 'S' or 'GT and has 6 numbers after that"),
    ROUTE_ALREADY_EXISTS("Such route already exists"),
    INCORRECT_ROUTE_INFO_INPUT("Please, check Note: "),
    TRAIN_TYPE_NOT_CORRESPONDS_WITH_TRAIN_TYPE("Train type must correspond with train number");



    private final String message;


    ErrorMessages(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
