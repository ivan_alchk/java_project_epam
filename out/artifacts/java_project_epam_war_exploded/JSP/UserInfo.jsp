<%--
  Created by IntelliJ IDEA.
  User: altsc
  Date: 7/9/2020
  Time: 4:59 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>UpdatePage</title>
</head>
<body>

<form action="userInfoController" method="post">
    <table>
        <tr>
            <td>
                <input type="submit" name="action" value="Delete your account"/><br/>
            </td>

        </tr>
        <tr>
            <td>
                <label>New username:
                    <input type="text" name="newUserName">
                </label>
                <input type="submit" name="action" value="Update username">
            </td>
        </tr>
        <tr>
            <td>
                <label>New password:
                    <input type="password" name="newPassword">
                </label>
                <input type="submit" name="action" value="Update password">
            </td>

        </tr>

    </table>

</form>

<div>
    <button onclick="location.href = '/java_project_epam_war_exploded/userMenuController'">Back to menu</button>
</div>

</body>
</html>
