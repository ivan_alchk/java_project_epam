<%--
  Created by IntelliJ IDEA.
  User: ivan
  Date: 24.03.20
  Time: 21:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>signUpActivity</title>
</head>
<body>
<form method="post">

    <table style="width: 50%">
        <tr>
            <td>
                <label>UserName:
                    <input type="text" name="userName"/><br/>
                </label>
            </td>
        </tr>

        <tr>
            <td>
                <label>Password:
                    <input type="password" name="pass"><br/>
                </label>
            </td>
        </tr>

        <tr>
            <td>
                <label>First Name:
                    <input type="text" name="firstName"><br/>
                </label>
            </td>
        </tr>

        <tr>
            <td>
                <label> Last Name:
                    <input type="text" name="lastName"><br/>

                </label>
            </td>
        </tr>
        <tr>
            <td>
                <label>Telephone:
                    <input type="text" name="telephoneNumber"><br/>
                </label>
            </td>

        </tr>

    </table>
    <button type="submit">Submit</button>
</form>


<%
    String invalidInput = (String) request.getAttribute("invalidInput");
    out.println(invalidInput == null ? "" : invalidInput);
    String correctInput = (String) request.getAttribute("correctInput");
    out.println(correctInput == null ? "" : "You`re successfully registered");


%>
<div>
    <button onclick="location.href = '/java_project_epam_war_exploded/logInController'">LogIn</button>
</div>

</body>
</html>
