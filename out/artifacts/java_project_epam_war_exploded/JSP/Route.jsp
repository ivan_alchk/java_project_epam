<%--
  Created by IntelliJ IDEA.
  User: altsc
  Date: 7/10/2020
  Time: 6:21 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<form method="post">
    <table style="width: 50%">
        <tr>
            <td>
                <label>RouteId:
                    <input type="text" name="routeId"/><br/>
                </label>
            </td>
        </tr>

        <tr>
            <td>
                <label>FromCity:
                    <input type="text" name="fromCity"><br/>
                </label>
            </td>
        </tr>
        <tr>
            <td>
                <label>ToCity:
                    <input type="text" name="toCity"><br/>
                </label>
            </td>

        </tr>
        <tr>
            <td>
                <label>DepartureTime:
                    <input type="text" name="departureTime"><br/>
                </label>
            </td>

        </tr>


        <tr>
            <td>
                <label>TrainNumber:
                    <input type="text" name="trainNumber"><br/>
                </label>
            </td>

        </tr>
        <tr>
            <td>
                <label>Distance:
                    <input type="text" name="distance"><br/>
                </label>
            </td>

        </tr>

    </table>
    <button type="submit">Submit</button>


</form>
<div>
    <button onclick="location.href = '/java_project_epam_war_exploded/adminMenuController'">Back to menu</button>
</div>
<%
    String info = (String) request.getAttribute("info");
    String error = (String) request.getAttribute("error");

    out.println(info != null ? info : "");
    out.println(error != null ? error : "");
%>

</body>
</html>
